# CDLM-Back

# Description

This repo contains all the back code for the site called Concept De La Muerte.

# Tech stack

* **Spring**
* **Hibernate**

# Contributors

* Romain Haas
* Emmanuel Pluot