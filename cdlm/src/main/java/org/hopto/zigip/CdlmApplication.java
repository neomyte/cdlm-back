package org.hopto.zigip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdlmApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdlmApplication.class, args);
	}

}
