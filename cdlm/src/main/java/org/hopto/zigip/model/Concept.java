package org.hopto.zigip.model;

import static org.hopto.zigip.tools.AnsibleState.CHANGED;
import static org.hopto.zigip.tools.AnsibleState.FAILURE;
import static org.hopto.zigip.tools.AnsibleState.OK;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotEmpty;

import org.hopto.zigip.tools.AnsibleState;
import org.hopto.zigip.tools.State;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Concept {
	@Id
	@GeneratedValue
	private int id;

	@NotEmpty
	@Column(unique = true)
	private String name;

	private boolean pure;

	private boolean dead;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name="Family", 
		joinColumns={@JoinColumn(name="ParentId")}, 
		inverseJoinColumns={@JoinColumn(name="ChildId")}
	)
	private List<Concept> parents;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Concept> children;
	
	public Concept(AnsibleState state){
		id = state == FAILURE ? -1 : -2;
	}
	
	public Concept(String name){
		this.name = name;
		this.parents = new ArrayList<Concept>();
		this.children = new ArrayList<Concept>();
	}
	
	public State addParent(Concept parent){
		if(parents.contains(parent)){
			return new State(OK, "Parent "+parent+" is already a parent of "+name+".");
		}else{
			parents.add(parent);
			return new State(CHANGED, "Added "+parent+" to "+name+"'s parents.");
		}
	}
}