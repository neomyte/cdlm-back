package org.hopto.zigip.web.controller;

import org.hopto.zigip.business.ConceptBusiness;
import org.hopto.zigip.tools.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "http://zigip.hopto.org:5642")
public class ConceptController {

	@Autowired
	private ConceptBusiness business;

	@ApiOperation(value = "Return the list of concepts")
	@GetMapping(value = "/read/concepts", produces = "application/json")
	public State listConcepts() {
		return business.listConcepts();
	}
	
	@ApiOperation(value = "Create a concept with the name {name}")
	@PostMapping(value = "/create/concept/name/{name}", produces = "application/json")
	public State createConcept(@PathVariable String name) {
		return business.createConcept(name);
	}
	
	@ApiOperation(value = "Create a concept with the name {name} and the given {parents}")
	@PostMapping(value = "/create/concept/name/{name}/parents/{parents}", produces = "application/json")
	public State createConcept(@PathVariable String name, @PathVariable String parents) {
		return business.createConcept(name, parents.split(","));
	}
}
