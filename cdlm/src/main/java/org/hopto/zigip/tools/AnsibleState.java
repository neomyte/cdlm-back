package org.hopto.zigip.tools;

public enum AnsibleState {
	OK, SKIPPED, CHANGED, FAILURE
}
