package org.hopto.zigip.tools;

import java.util.HashMap;
import java.util.Map;

public class State {

	String message_;
	AnsibleState ansibleState_;
	HashMap<String, Object> information_;

	public State(AnsibleState ansibleState, String message){
		message_ = message;
		ansibleState_ = ansibleState;
		information_ = new HashMap<>();
	}
	
	public String getMessage(){
		return message_;
	}
	
	public AnsibleState getAnsibleState(){
		return ansibleState_;
	}

	public Map<String, Object> getInformation(){
		return information_;
	}
	
	public State addInformation(String name, Object data){
		information_.put(name, data);
		return this;
	}
	
	public Object getInformation(String name){
		return information_.get(name);
	}
	
}
