package org.hopto.zigip.business.dao;

import org.hopto.zigip.model.Concept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

@Repository
public interface ConceptDao extends JpaRepository<Concept, Integer> {
	
	@Nullable
	public Concept getByName(String name);

}