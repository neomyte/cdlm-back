package org.hopto.zigip.business;

import static org.hopto.zigip.tools.AnsibleState.CHANGED;
import static org.hopto.zigip.tools.AnsibleState.FAILURE;
import static org.hopto.zigip.tools.AnsibleState.OK;

import org.hopto.zigip.business.dao.ConceptDao;
import org.hopto.zigip.model.Concept;
import org.hopto.zigip.tools.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConceptBusiness {
	@Autowired
	ConceptDao conceptDao;
	
	private boolean conceptExists(String name){
		return conceptDao.getByName(name) != null;
	}
	
	private int getConceptId(String name){
		if(!conceptExists(name)) return -1;
		return conceptDao.getByName(name).getId();
	}
	
	public State listConcepts() {
		return new State(OK, "").addInformation("list", conceptDao.findAll());
	}

	public State createConcept(String name, String... parents) {
		if(conceptExists(name)){
			return new State(OK, "Concept "+name+" already present.").addInformation("id", getConceptId(name));
		}
		Concept newConcept = new Concept(name);
		for(String parent: parents){
			if(!conceptExists(parent)){
				return new State(FAILURE, "Cannot add "+parent+" as a parent to "+name+" for it is not a known Concept itself.");
			}
			State state = newConcept.addParent(conceptDao.getByName(parent));
			if(state.getAnsibleState() == FAILURE){
				return state;
			}
		}
		conceptDao.saveAndFlush(newConcept);
		return new State(CHANGED, "Concept "+name).addInformation("id", getConceptId(name));
	}

	public State removeConcept(int id) {
		return new State(OK, "Concept ");
	}

	public State removeConcept(String name) {
		return new State(OK, "Concept ");
	}

	public Concept getConcept(int id) {
		return new Concept();
	}

	public Concept getConcept(String name) {
		return new Concept();
	}

	public State setConceptName(int id, String name) {
		return new State(OK, "Concept ");
	}

	public State setConceptName(String oldName, String newName) {
		return new State(OK, "Concept ");
	}
}
